import { TestHelper } from '../testhelper';
import App from '../../src/App';
import * as chai from 'chai';
import chaiHttp = require('chai-http');

chai.use(chaiHttp);
const session = new TestHelper();

afterAll(async () => {
  await session.currentConnection.close();
});

beforeEach(async () => await session.connectToDatabase());

test('route CRUD', async () => {
  // test user
  const user = await session.generateNewUser();

  // create
  const route = {
    coordinates: [
      { long: session.randomString(), lat: session.randomString() },
    ],
    difficulty: Math.random() * 100,
    note: session.randomString(),
    private: true,
    title: session.randomString(),
    userId: user.id,
  };

  const testRoute = await chai
    .request(App)
    .post(`/v1/routes`)
    .send(route)
    .then((res) => res.body);
  expect(testRoute.id).not.toBeUndefined();
  expect(testRoute.title).toBe(route.title);
  expect(testRoute.difficulty).toBe(route.difficulty);
  expect(testRoute.private).toBe(true),
  expect(testRoute.note).toBe(route.note);
  expect(testRoute.upVotes).toBe(0);
  expect(testRoute.downVotes).toBe(0);
  expect(testRoute.coordinates).toEqual(route.coordinates);

  // Read User
  const foundRoute = await chai
    .request(App)
    .get(`/v1/routes/${testRoute.id}`)
    .then((res) => res.body);
  expect(foundRoute.id).toBe(testRoute.id);
  expect(foundRoute.coordinates).toEqual(testRoute.coordinates);
  expect(foundRoute.title).toBe(testRoute.title);

  // Update User
  const newInfo = {
    coordinates: [
      { long: session.randomString(), lat: session.randomString() },
      { long: session.randomString(), lat: session.randomString() },
      { long: session.randomString(), lat: session.randomString() },
    ],
    difficulty: 2,
    downVotes: 9,
    note: session.randomString(),
    private: false,
    upVotes: 3,
  };

  const updatedRoute = await chai
    .request(App)
    .put(`/v1/routes/${foundRoute.id}`)
    .send(newInfo)
    .then((res) => res.body);
  expect(updatedRoute.id).toBe(foundRoute.id);
  expect(updatedRoute.coordinates).toEqual(newInfo.coordinates);
  expect(updatedRoute.upVotes).toBe(newInfo.upVotes);
  expect(updatedRoute.downVotes).toBe(newInfo.downVotes);
  expect(updatedRoute.note).toBe(newInfo.note);
  expect(updatedRoute.difficulty).toBe(newInfo.difficulty);

  // Destroy User
  const deletedRoute = await chai
    .request(App)
    .del(`/v1/routes/${updatedRoute.id}`)
    .then((res) => res.body);
  expect(deletedRoute.id).toBeUndefined();

  // Read Destroyed user
  const error = await chai
    .request(App)
    .get(`/v1/routes/${updatedRoute.id}`)
    .catch((e) => e.response);
  expect(error.status).toBe(404);
});
