import { TestHelper } from '../testhelper';
import App from '../../src/App';
import * as chai from 'chai';
import chaiHttp = require('chai-http');

chai.use(chaiHttp);
const session = new TestHelper();

afterAll(async () => {
  await session.currentConnection.close();
});

beforeEach(async () => await session.connectToDatabase());

test('user CRUD', async () => {
  // create
  const user = {
    email: session.randomString(),
    password: session.randomString(),
    username: session.randomString(),
  };

  const testUser = await chai
    .request(App)
    .post(`/v1/users`)
    .send(user)
    .then((res) => res.body);
  expect(testUser.username).toBe(user.username);
  expect(testUser.completedRoutes).toBe(0);
  expect(testUser.bikeInfo).toBe('');
  expect(testUser.phone).toBe('');
  expect(testUser.dateOfBirth).toBe(null);
  expect(testUser.id).not.toBeUndefined();

  // Read User
  const foundUser = await chai
    .request(App)
    .get(`/v1/users/${testUser.id}`)
    .then((res) => res.body);
  expect(foundUser.username).toBe(testUser.username);

  // Find user by username and password
  const params = {
    password: user.password,
    username: testUser.username,
  };

  const temp = await chai
    .request(App)
    .post(`/v1/users/authenticate`)
    .send(params)
    .then((res) => res.body);
  expect(temp.username).toBe(testUser.username);

  // Update User
  const newInfo = {
      bikeInfo: session.randomString(),
      completedRoutes: 19,
      dateOfBirth: '1994-09-10T06:00:00.000Z',
      password: session.randomString(),
    };

  const updatedUser = await chai
    .request(App)
    .put(`/v1/users/${foundUser.id}`)
    .send(newInfo)
    .then((res) => res.body);
  expect(updatedUser.password).toBe(newInfo.password);
  expect(updatedUser.id).toBe(foundUser.id);
  expect(updatedUser.bikeInfo).toBe(newInfo.bikeInfo);
  expect(updatedUser.completedRoutes).toBe(newInfo.completedRoutes);

  // Destroy User
  const deletedUser = await chai
    .request(App)
    .del(`/v1/users/${updatedUser.id}`)
    .then((res) => res.body);
  expect(deletedUser.id).toBeUndefined();
  expect(deletedUser.username).toBe(updatedUser.username);
  expect(deletedUser.email).toBe(updatedUser.email);

  // Read Destroyed user
  const error = await chai
  .request(App)
  .get(`/v1/users/${updatedUser.id}`)
  .catch((e) => e.response);
  expect(error.status).toBe(404);
});
