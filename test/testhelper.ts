import App from '../src/App';
import * as db from './ormtestconfig';
import { Connection, createConnection } from 'typeorm';
import * as http from 'http';
import { IUser } from '../src/models/User';
import * as chai from 'chai';
import chaiHttp = require('chai-http');

chai.use(chaiHttp);

export class TestHelper {
  public port = process.env.PORT || 3000;
  public currentConnection: Connection;
  public baseUrl = 'http://127.0.0.1:3000/v1';
  public server: any;

  public randomString(): string {
    return Math.random()
      .toString(36)
      .substring(2);
  }

  public connectToDatabase(): Promise<void> {
    return createConnection(db.default)
      .then(async (connection) => {
        this.currentConnection = connection;
        // await connection.runMigrations();

        console.log('Type ORM Connection established');
      })
      .catch((error) => console.error('TypeORM connection error: ', error));
  }

  public startServer(): void {
    this.server = http.createServer(App);

    this.server.listen(this.port, async () => {
      console.log('Server listening on port 3000');
    });
  }

  public closeServer(): void {
      this.server.close();
  }

  public async generateNewUser(): Promise<IUser> {
    const user = {
      email: this.randomString(),
      password: this.randomString(),
      username: this.randomString(),
    };

    return await chai
      .request(App)
      .post(`/v1/users`)
      .send(user)
      .then((res) => res.body);
  }

}
