import { User } from '../src/models/User';
import { Route } from '../src/models/Route';
import { RoutePhoto } from '../src/models/RoutePhoto';
import { ConnectionOptions } from 'typeorm';

const connectionOptions: ConnectionOptions = {
    type: 'mysql',
    // tslint:disable-next-line:object-literal-sort-keys
    host: 'mysql',
    port: 3306,
    username: 'root',
    password: 'password',
    database: 'icycleTest',
    synchronize: true,
    logging: false,
    entities: [
        User,
        Route,
        RoutePhoto,
    ],
    // migrations: [
    //     ./dist/migrations/*.js
    // ],
};

export default connectionOptions;
