import { Entity, PrimaryGeneratedColumn, Column, ManyToOne, OneToMany, CreateDateColumn, UpdateDateColumn, ManyToMany } from 'typeorm';
import {User} from './User';
import { RoutePhoto } from './RoutePhoto';

export interface IRoute {
    id: number;
    private: boolean;
    title: string;
    note: string;
    distance: number;
    routePins: JSON;
    pointPins: JSON;
    difficulty: number;
    upVotes: number;
    downVotes: number;
    createdTime: Date;
    updateTime: Date;
}

@Entity()
export class Route {
    @PrimaryGeneratedColumn()
    public id: number; // The id assigned to the route automatically

    @Column()
    public private: boolean; // If the route is private

    @Column()
    public title: string; // The title of the route

    @Column()
    public distance: number; // 1 - 3: Easy, Medium, Hard

    @Column({
        type: 'json',
    })
    public routePins: JSON; // JSON Array for storing the pins that define the route that the user will take

    @Column({
        type: 'json',
    })
    public pointPins: JSON; // JSON Array for storing the pins signifying points of interest for the route

    @Column({
        default: '',
    })
    public note: string; // For extra information about the route

    @Column()
    public difficulty: number; // 1 - 3: Easy, Medium, Hard

    @Column({
        default: 0,
    })
    public upVotes: number; // The number of times the route has been upVoted

    @Column({
        default: 0,
    })
    public downVotes: number; // The number of times the route has been downVoted

    @OneToMany(() => RoutePhoto, (routePhoto) => routePhoto.route)
    public photos: RoutePhoto[]; // The photos belonging to the route

    @ManyToOne(() => User, (user) => user.routes)
    public user: User;

    // Auto
    @CreateDateColumn()
    public createdTime: Date;

    @UpdateDateColumn()
    public updateTime: Date;
}
