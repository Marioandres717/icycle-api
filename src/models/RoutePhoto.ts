import { Entity, PrimaryGeneratedColumn, Column, CreateDateColumn, ManyToOne, ManyToMany, JoinTable } from 'typeorm';
import { Route } from './Route';
import { User } from './User';

export interface IRoutePhoto {
    id: number;
    photo: string;
    caption: string;
    coordinates: JSON;
    postedDate: Date;
}

@Entity()
export class RoutePhoto {
    @PrimaryGeneratedColumn()
    public id: number;

    @Column()
    public photo: string; // The URL of the photo

    @Column({
        type: 'json',
    })
    public coordinates: JSON; // The selected location of the photo

    @Column()
    public title: string; // A user-defined title for the photo

    @Column()
    public caption: string; // A user-defined caption for the photo

    @ManyToOne(() => Route, (route) => route.photos)
    public route: Route; // Many photos ca belong to one route

    @ManyToOne(() => User, (user) => user.photos)
    public user: User; // Many photos can belong to one user

    // Auto
    @CreateDateColumn()
    public postedDate: Date;
}
