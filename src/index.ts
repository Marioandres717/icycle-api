import * as http from 'http';
import 'reflect-metadata';
import App from './App';
import {createConnection} from 'typeorm';

const server = http.createServer(App);
const port = process.env.PORT || 3000;

server.listen(port, async () => {
  console.log('Server listening on port 3000');
});

// TypeORM connection
createConnection().then(async (connection) => {
  console.log('TypeORM connection established.');
  // await connection.runMigrations();
}).catch((error) => console.error('TypeORM connection error: ', error));
