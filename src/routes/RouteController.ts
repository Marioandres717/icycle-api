import * as boom from 'boom';
import { NextFunction, Request, Response, Router } from 'express';
import { IRouteService } from '../services/interfaces/IRouteService';

export class RouteController {

    public static create(router: Router, service: IRouteService): void {

        const instance = new RouteController(service);

        router.get('/v1/routes/:id', (req: Request, res: Response, next: NextFunction) => {
            instance.getRoute(req, res, next);
        });

        router.get('/v1/routes', (req: Request, res: Response, next: NextFunction) => {
            instance.getAllRoute(req, res, next);
        });

        router.get('/v1/routes/user/:userId', (req: Request, res: Response, next: NextFunction) => {
            instance.getUserRoutes(req, res, next);
        });

        router.get('/v1/routes/user/:userId/saved', (req: Request, res: Response, next: NextFunction) => {
            instance.getUserSavedRoutes(req, res, next);
        });

        router.get('/v1/routes/:id/user/:userId/hasSaved', (req: Request, res: Response, next: NextFunction) => {
            instance.hasSaved(req, res, next);
        });

        router.get('/v1/routes/:id/user/:userId/hasVoted', (req: Request, res: Response, next: NextFunction) => {
            instance.hasVoted(req, res, next);
        });

        router.post('/v1/routes', (req: Request, res: Response, next: NextFunction) => {
            instance.addRoute(req, res, next);
        });

        router.put('/v1/routes/:id', (req: Request, res: Response, next: NextFunction) => {
            instance.updateRoute(req, res, next);
        });

        router.put('/v1/routes/:id/user/:userId/save', (req: Request, res: Response, next: NextFunction) => {
            instance.saveRoute(req, res, next);
        });

        router.put('/v1/routes/:id/upVote/:userId', (req: Request, res: Response, next: NextFunction) => {
            instance.upVoteRoute(req, res, next);
        });

        router.put('/v1/routes/:id/downVote/:userId', (req: Request, res: Response, next: NextFunction) => {
            instance.downVoteRoute(req, res, next);
        });

        router.delete('/v1/routes/:id', (req: Request, res: Response, next: NextFunction) => {
            instance.deleteRoute(req, res, next);
        });
    }

    public service: IRouteService;

    constructor(service: IRouteService) {
        this.service = service;
    }

    // tslint:disable-next-line:no-unused-variable
    public async getRoute(req: Request, res: Response, next: NextFunction): Promise<void> {
        try {
            const user = await this.service.findOne(req.params.id);
            res.send(user);
        } catch (error) {
            next(boom.boomify(error, { statusCode: error.statusCode ? error.statusCode : 404, message: 'Cannot read route.' }));
        }
    }

    public async getAllRoute(req: Request, res: Response, next: NextFunction): Promise<void> {
        try {
            const users = await this.service.findAll();
            res.send(users);
        } catch (error) {
            next(boom.boomify(error, { statusCode: error.statusCode ? error.statusCode : 404, message: 'Cannot read  routes.' }));
        }
    }

    public async getUserRoutes(req: Request, res: Response, next: NextFunction): Promise<void> {
        try {
            const users = await this.service.findUserRoutes(req.params.userId);
            res.send(users);
        } catch (error) {
            next(boom.boomify(error, { statusCode: error.statusCode ? error.statusCode : 404, message: 'Cannot read  route.' }));
        }
    }

    public async getUserSavedRoutes(req: Request, res: Response, next: NextFunction): Promise<void> {
        try {
            const routes = await this.service.findUserSavedRoutes(req.params.userId);
            res.send(routes);
        } catch (error) {
            next(boom.boomify(error, { statusCode: error.statusCode ? error.statusCode : 404, message: 'Cannot read route.' }));
        }
    }

    public async saveRoute(req: Request, res: Response, next: NextFunction): Promise<void> {
        try {
            const success = await this.service.saveRoute(req.params.id, req.params.userId);
            res.send(success);
        } catch (error) {
            next(boom.boomify(error, { statusCode: error.statusCode ? error.statusCode : 404, message: 'Cannot read route.' }));
        }
    }

    public async addRoute(req: Request, res: Response, next: NextFunction): Promise<void> {
        try {
            const user = await this.service.add(req.body);
            res.send(user);
        } catch (error) {
            next(boom.boomify(error, { statusCode: error.statusCode ? error.statusCode : 404, message: 'Cannot create route.' }));
        }
    }

    public async upVoteRoute(req: Request, res: Response, next: NextFunction): Promise<void> {
        try {
            const success = await this.service.upVoteRoute(req.params.id, req.params.userId);
            res.send(success);
        } catch (error) {
            next(boom.boomify(error, { statusCode: error.statusCode ? error.statusCode : 404, message: 'Cannot upVote route.' }));
        }
    }

    public async downVoteRoute(req: Request, res: Response, next: NextFunction): Promise<void> {
        try {
            const success = await this.service.downVoteRoute(req.params.id, req.params.userId);
            res.send(success);
        } catch (error) {
            next(boom.boomify(error, { statusCode: error.statusCode ? error.statusCode : 404, message: 'Cannot downVote route.' }));
        }
    }

    public async updateRoute(req: Request, res: Response, next: NextFunction): Promise<void> {
        try {
            const user = await this.service.update(req.params.id, req.body);
            res.send(user);
        } catch (error) {
            next(boom.boomify(error, { statusCode: error.statusCode ? error.statusCode : 404, message: 'Cannot update route.' }));
        }
    }

    public async deleteRoute(req: Request, res: Response, next: NextFunction): Promise<void> {
        try {
            const user = await this.service.delete(req.params.id);
            res.send(user);
        } catch (error) {
            next(boom.boomify(error, { statusCode: error.statusCode ? error.statusCode : 404, message: 'Cannot destroy route.' }));
        }
    }

    public async hasVoted(req: Request, res: Response, next: NextFunction): Promise<void> {
        try {
            const voted = await this.service.hasVoted(req.params.id, req.params.userId);
            res.send(voted);
        } catch (error) {
            next(boom.boomify(error, { statusCode: error.statusCode ? error.statusCode : 404, message: 'Cannot destroy route.' }));
        }
    }

    public async hasSaved(req: Request, res: Response, next: NextFunction): Promise<void> {
        try {
            const saved = await this.service.hasSaved(req.params.id, req.params.userId);
            res.send(saved);
        } catch (error) {
            next(boom.boomify(error, { statusCode: error.statusCode ? error.statusCode : 404, message: 'Cannot destroy route.' }));
        }
    }
}
