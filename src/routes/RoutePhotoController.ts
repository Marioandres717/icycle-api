import * as boom from 'boom';
import { NextFunction, Request, Response, Router } from 'express';
import { IRoutePhotoService } from '../services/interfaces/IRoutePhotoService';

export class RoutePhotoController {

    public static create(router: Router, service: IRoutePhotoService): void {

        const instance = new RoutePhotoController(service);

        router.get('/v1/routes/:id/photo/:photoId', (req: Request, res: Response, next: NextFunction) => {
            instance.getRoutePhoto(req, res, next);
        });

        router.get('/v1/routes/:id/photo', (req: Request, res: Response, next: NextFunction) => {
            instance.getAllRoutePhotos(req, res, next);
        });

        router.get('/v1/user/:userId/photo', (req: Request, res: Response, next: NextFunction) => {
            instance.getAllUsersPhotos(req, res, next);
        });

        router.post('/v1/routes/:id/user/:userId/photo', (req: Request, res: Response, next: NextFunction) => {
            instance.addRoutePhoto(req, res, next);
        });

        router.put('/v1/routes/:id/photo/:photoId', (req: Request, res: Response, next: NextFunction) => {
            instance.updateRoutePhoto(req, res, next);
        });

        router.delete('/v1/routes/:id/photo/:photoId', (req: Request, res: Response, next: NextFunction) => {
            instance.deleteRoutePhoto(req, res, next);
        });
    }

    public service: IRoutePhotoService;

    constructor(service: IRoutePhotoService) {
        this.service = service;
    }

    // tslint:disable-next-line:no-unused-variable
    public async getRoutePhoto(req: Request, res: Response, next: NextFunction): Promise<void> {
        try {
            const photo = await this.service.findOne(req.params.id, req.params.photoId);
            res.send(photo);
        } catch (error) {
            next(boom.boomify(error, { statusCode: error.statusCode ? error.statusCode : 404, message: 'Cannot read route photo.' }));
        }
    }

    public async getAllRoutePhotos(req: Request, res: Response, next: NextFunction): Promise<void> {
        try {
            const photos = await this.service.findAll(req.params.id);
            res.send(photos);
        } catch (error) {
            next(boom.boomify(error, { statusCode: error.statusCode ? error.statusCode : 404, message: 'Cannot read route photo.' }));
        }
    }

    public async addRoutePhoto(req: Request, res: Response, next: NextFunction): Promise<void> {
        try {
            const photo = await this.service.add(req.params.id, req.params.userId, req.body);
            res.send(photo);
        } catch (error) {
            next(boom.boomify(error, { statusCode: error.statusCode ? error.statusCode : 404, message: 'Cannot create route photo.' }));
        }
    }

    public async getAllUsersPhotos(req: Request, res: Response, next: NextFunction): Promise<void> {
        try {
            const photos = await this.service.getAllUsersPhotos(req.params.userId);
            res.send(photos);
        } catch (error) {
            next(boom.boomify(error, { statusCode: error.statusCode ? error.statusCode : 404, message: 'Cannot read route photo.' }));
        }
    }

    public async updateRoutePhoto(req: Request, res: Response, next: NextFunction): Promise<void> {
        try {
            const photo = await this.service.update(req.params.id, req.params.photoId, req.body);
            res.send(photo);
        } catch (error) {
            next(boom.boomify(error, { statusCode: error.statusCode ? error.statusCode : 404, message: 'Cannot update route photo.' }));
        }
    }

    public async deleteRoutePhoto(req: Request, res: Response, next: NextFunction): Promise<void> {
        try {
            const user = await this.service.delete(req.params.id, req.params.photoId);
            res.send(user);
        } catch (error) {
            next(boom.boomify(error, { statusCode: error.statusCode ? error.statusCode : 404, message: 'Cannot destroy route photo.' }));
        }
    }
}
