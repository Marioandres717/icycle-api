import * as boom from 'boom';
import { NextFunction, Request, Response, Router } from 'express';
import { IUserService } from '../services/interfaces/IUserService';

export class UserController {

    public static create(router: Router, service: IUserService): void {

        const instance = new UserController(service);

        router.get('/v1/users/:id', (req: Request, res: Response, next: NextFunction) => {
            instance.getUser(req, res, next);
        });

        router.get('/v1/users', (req: Request, res: Response, next: NextFunction) => {
            instance.getAllUser(req, res, next);
        });

        router.post('/v1/users/authenticate', (req: Request, res: Response, next: NextFunction) => {
            instance.getUserByUsernameAndPassword(req, res, next);
        });

        router.post('/v1/users', (req: Request, res: Response, next: NextFunction) => {
            instance.addUser(req, res, next);
        });

        router.put('/v1/users/:id', (req: Request, res: Response, next: NextFunction) => {
            instance.updateUser(req, res, next);
        });

        router.delete('/v1/users/:id', (req: Request, res: Response, next: NextFunction) => {
            instance.deleteUser(req, res, next);
        });

    }

    public service: IUserService;

    constructor(service: IUserService) {
        this.service = service;
    }

    // tslint:disable-next-line:no-unused-variable
    public async getUser(req: Request, res: Response, next: NextFunction): Promise<void> {
        try {
            const user = await this.service.findOne(req.params.id);
            res.send(user);
        } catch (error) {
            next(boom.boomify(error, { statusCode: error.statusCode ? error.statusCode : 404, message: 'Cannot read user.' }));
        }
    }

    public async getAllUser(req: Request, res: Response, next: NextFunction): Promise<void> {
        try {
            const users = await this.service.findAll();
            res.send(users);
        } catch (error) {
            next(boom.boomify(error, { statusCode: error.statusCode ? error.statusCode : 404, message: 'Cannot read users.' }));
        }
    }

    public async getUserByUsernameAndPassword(req: Request, res: Response, next: NextFunction): Promise<void> {
        try {
            const user = await this.service.getUserByUsernameAndPassword(req.body.username, req.body.password);
            res.send(user);
        } catch (error) {
            next(boom.boomify(error, { statusCode: error.statusCode ? error.statusCode : 404, message: 'Cannot retrieve user by username and password.' }));
        }
    }

    public async addUser(req: Request, res: Response, next: NextFunction): Promise<void> {
        try {
            const user = await this.service.add(req.body);
            res.send(user);
        } catch (error) {
            next(boom.boomify(error, { statusCode: error.statusCode ? error.statusCode : 404, message: 'Cannot create user.' }));
        }
    }

    public async updateUser(req: Request, res: Response, next: NextFunction): Promise<void> {
        try {
            const user = await this.service.update(req.params.id, req.body);
            res.send(user);
        } catch (error) {
            next(boom.boomify(error, { statusCode: error.statusCode ? error.statusCode : 404, message: 'Cannot update user.' }));
        }
    }

    public async deleteUser(req: Request, res: Response, next: NextFunction): Promise<void> {
        try {
            const user = await this.service.delete(req.params.id);
            res.send(user);
        } catch (error) {
            next(boom.boomify(error, { statusCode: error.statusCode ? error.statusCode : 404, message: 'Cannot destroy user.' }));
        }
    }
}
