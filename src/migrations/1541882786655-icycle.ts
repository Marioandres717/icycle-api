/*tslint:disable */
import {MigrationInterface, QueryRunner} from "typeorm";

export class icycle1541882786655 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query("ALTER TABLE `route` CHANGE `createdTime` `createdTime` datetime NOT NULL");
        await queryRunner.query("ALTER TABLE `route` CHANGE `updateTime` `updateTime` datetime NOT NULL");
        await queryRunner.query("ALTER TABLE `user` CHANGE `dateOfBirth` `dateOfBirth` datetime NOT NULL");
        await queryRunner.query("ALTER TABLE `user` CHANGE `createdTime` `createdTime` datetime NOT NULL");
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query("ALTER TABLE `user` CHANGE `createdTime` `createdTime` datetime(0) NOT NULL");
        await queryRunner.query("ALTER TABLE `user` CHANGE `dateOfBirth` `dateOfBirth` datetime(0) NOT NULL");
        await queryRunner.query("ALTER TABLE `route` CHANGE `updateTime` `updateTime` datetime(0) NOT NULL");
        await queryRunner.query("ALTER TABLE `route` CHANGE `createdTime` `createdTime` datetime(0) NOT NULL");
    }

}
