/* tslint:disable*/

import {MigrationInterface, QueryRunner} from "typeorm";

export class icycle1541817539746 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query("CREATE TABLE `route_pin` (`id` int NOT NULL AUTO_INCREMENT, `coordinates` json NOT NULL, `title` varchar(255) NOT NULL, `note` varchar(255) NOT NULL, `userId` int NULL, `routeId` int NULL, PRIMARY KEY (`id`)) ENGINE=InnoDB");
        await queryRunner.query("CREATE TABLE `route_comment` (`id` int NOT NULL AUTO_INCREMENT, `content` varchar(255) NOT NULL, `postedDate` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6), `routeId` int NULL, PRIMARY KEY (`id`)) ENGINE=InnoDB");
        await queryRunner.query("CREATE TABLE `route_photo` (`id` int NOT NULL AUTO_INCREMENT, `photo` varchar(255) NOT NULL, `caption` varchar(255) NOT NULL, `postedDate` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6), `routeId` int NULL, PRIMARY KEY (`id`)) ENGINE=InnoDB");
        await queryRunner.query("CREATE TABLE `route` (`id` int NOT NULL AUTO_INCREMENT, `private` tinyint NOT NULL, `title` varchar(255) NOT NULL, `coordinates` json NOT NULL, `createdTime` datetime NOT NULL, `updateTime` datetime NOT NULL, `note` varchar(255) NOT NULL, `difficulty` int NOT NULL, `upVotes` int NOT NULL, `downVotes` int NOT NULL, `userId` int NULL, PRIMARY KEY (`id`)) ENGINE=InnoDB");
        await queryRunner.query("CREATE TABLE `billet` (`id` int NOT NULL AUTO_INCREMENT, `coordinates` json NOT NULL, `title` varchar(255) NOT NULL, `note` varchar(255) NOT NULL, `rating` int NOT NULL, `countOfRatings` int NOT NULL, `userId` int NULL, PRIMARY KEY (`id`)) ENGINE=InnoDB");
        await queryRunner.query("CREATE TABLE `route_comment_user_user` (`routeCommentId` int NOT NULL, `userId` int NOT NULL, PRIMARY KEY (`routeCommentId`, `userId`)) ENGINE=InnoDB");
        await queryRunner.query("CREATE TABLE `route_photo_user_user` (`routePhotoId` int NOT NULL, `userId` int NOT NULL, PRIMARY KEY (`routePhotoId`, `userId`)) ENGINE=InnoDB");
        await queryRunner.query("ALTER TABLE `user` DROP COLUMN `firstName`");
        await queryRunner.query("ALTER TABLE `user` DROP COLUMN `lastName`");
        await queryRunner.query("ALTER TABLE `user` DROP COLUMN `age`");
        await queryRunner.query("ALTER TABLE `user` ADD `username` varchar(255) NOT NULL");
        await queryRunner.query("ALTER TABLE `user` ADD `email` varchar(255) NOT NULL");
        await queryRunner.query("ALTER TABLE `user` ADD `dateOfBirth` datetime NOT NULL");
        await queryRunner.query("ALTER TABLE `user` ADD `password` varchar(255) NOT NULL");
        await queryRunner.query("ALTER TABLE `user` ADD `createdTime` datetime NOT NULL");
        await queryRunner.query("ALTER TABLE `user` ADD `completedRoutes` int NOT NULL");
        await queryRunner.query("ALTER TABLE `user` ADD `bikeInfo` varchar(255) NOT NULL");
        await queryRunner.query("ALTER TABLE `user` ADD `phone` varchar(255) NOT NULL");
        await queryRunner.query("CREATE UNIQUE INDEX `IDX_f4ca2c1e7c96ae6e8a7cca9df8` ON `user`(`username`, `email`)");
        await queryRunner.query("ALTER TABLE `route_pin` ADD CONSTRAINT `FK_e12ab75bc2ef7e4370c5a18ede6` FOREIGN KEY (`userId`) REFERENCES `user`(`id`)");
        await queryRunner.query("ALTER TABLE `route_pin` ADD CONSTRAINT `FK_ce2d05876ad08be2955f6ab0e65` FOREIGN KEY (`routeId`) REFERENCES `route`(`id`)");
        await queryRunner.query("ALTER TABLE `route_comment` ADD CONSTRAINT `FK_d2583bbeb7af9c9e2328ddb9421` FOREIGN KEY (`routeId`) REFERENCES `route`(`id`)");
        await queryRunner.query("ALTER TABLE `route_photo` ADD CONSTRAINT `FK_5ea5487c8fec309f920c032d50b` FOREIGN KEY (`routeId`) REFERENCES `route`(`id`)");
        await queryRunner.query("ALTER TABLE `route` ADD CONSTRAINT `FK_5f1f8af943496a71fa29f6a44f9` FOREIGN KEY (`userId`) REFERENCES `user`(`id`)");
        await queryRunner.query("ALTER TABLE `billet` ADD CONSTRAINT `FK_89bd59a81c944542c5d79b22179` FOREIGN KEY (`userId`) REFERENCES `user`(`id`)");
        await queryRunner.query("ALTER TABLE `route_comment_user_user` ADD CONSTRAINT `FK_ff7e6366b5a938828ae91595537` FOREIGN KEY (`routeCommentId`) REFERENCES `route_comment`(`id`) ON DELETE CASCADE");
        await queryRunner.query("ALTER TABLE `route_comment_user_user` ADD CONSTRAINT `FK_1a4cabfbcdf7f39bd3588d9f607` FOREIGN KEY (`userId`) REFERENCES `user`(`id`) ON DELETE CASCADE");
        await queryRunner.query("ALTER TABLE `route_photo_user_user` ADD CONSTRAINT `FK_92b2fef3e12cd0a7705fcbf4780` FOREIGN KEY (`routePhotoId`) REFERENCES `route_photo`(`id`) ON DELETE CASCADE");
        await queryRunner.query("ALTER TABLE `route_photo_user_user` ADD CONSTRAINT `FK_3eb1f334ba7a0ea01958e80bb0a` FOREIGN KEY (`userId`) REFERENCES `user`(`id`) ON DELETE CASCADE");
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query("ALTER TABLE `route_photo_user_user` DROP FOREIGN KEY `FK_3eb1f334ba7a0ea01958e80bb0a`");
        await queryRunner.query("ALTER TABLE `route_photo_user_user` DROP FOREIGN KEY `FK_92b2fef3e12cd0a7705fcbf4780`");
        await queryRunner.query("ALTER TABLE `route_comment_user_user` DROP FOREIGN KEY `FK_1a4cabfbcdf7f39bd3588d9f607`");
        await queryRunner.query("ALTER TABLE `route_comment_user_user` DROP FOREIGN KEY `FK_ff7e6366b5a938828ae91595537`");
        await queryRunner.query("ALTER TABLE `billet` DROP FOREIGN KEY `FK_89bd59a81c944542c5d79b22179`");
        await queryRunner.query("ALTER TABLE `route` DROP FOREIGN KEY `FK_5f1f8af943496a71fa29f6a44f9`");
        await queryRunner.query("ALTER TABLE `route_photo` DROP FOREIGN KEY `FK_5ea5487c8fec309f920c032d50b`");
        await queryRunner.query("ALTER TABLE `route_comment` DROP FOREIGN KEY `FK_d2583bbeb7af9c9e2328ddb9421`");
        await queryRunner.query("ALTER TABLE `route_pin` DROP FOREIGN KEY `FK_ce2d05876ad08be2955f6ab0e65`");
        await queryRunner.query("ALTER TABLE `route_pin` DROP FOREIGN KEY `FK_e12ab75bc2ef7e4370c5a18ede6`");
        await queryRunner.query("DROP INDEX `IDX_f4ca2c1e7c96ae6e8a7cca9df8` ON `user`");
        await queryRunner.query("ALTER TABLE `user` DROP COLUMN `phone`");
        await queryRunner.query("ALTER TABLE `user` DROP COLUMN `bikeInfo`");
        await queryRunner.query("ALTER TABLE `user` DROP COLUMN `completedRoutes`");
        await queryRunner.query("ALTER TABLE `user` DROP COLUMN `createdTime`");
        await queryRunner.query("ALTER TABLE `user` DROP COLUMN `password`");
        await queryRunner.query("ALTER TABLE `user` DROP COLUMN `dateOfBirth`");
        await queryRunner.query("ALTER TABLE `user` DROP COLUMN `email`");
        await queryRunner.query("ALTER TABLE `user` DROP COLUMN `username`");
        await queryRunner.query("ALTER TABLE `user` ADD `age` int NOT NULL");
        await queryRunner.query("ALTER TABLE `user` ADD `lastName` varchar(255) NOT NULL");
        await queryRunner.query("ALTER TABLE `user` ADD `firstName` varchar(255) NOT NULL");
        await queryRunner.query("DROP TABLE `route_photo_user_user`");
        await queryRunner.query("DROP TABLE `route_comment_user_user`");
        await queryRunner.query("DROP TABLE `billet`");
        await queryRunner.query("DROP TABLE `route`");
        await queryRunner.query("DROP TABLE `route_photo`");
        await queryRunner.query("DROP TABLE `route_comment`");
        await queryRunner.query("DROP TABLE `route_pin`");
    }

}
