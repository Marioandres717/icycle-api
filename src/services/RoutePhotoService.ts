import { getRepository } from 'typeorm';
import * as boom from 'boom';
import { Route } from '../models/Route';
import { User } from '../models/User';
import { IRoutePhoto, RoutePhoto } from '../models/RoutePhoto';
import { IRoutePhotoService } from './interfaces/IRoutePhotoService';

export class RoutePhotoService implements IRoutePhotoService {

    public async findOne(id: number, photoId: number): Promise<IRoutePhoto>  {
        return await getRepository(RoutePhoto)
        .createQueryBuilder('routePhoto')
        .where('routePhoto.id = :photoId', { photoId })
        .getOne();
    }

    public async findAll(routeId: number): Promise<IRoutePhoto[]>  {
        return await getRepository(RoutePhoto)
        .createQueryBuilder('routePhoto')
        .select(['routePhoto', 'user', 'route.id'])
        .where('routePhoto.routeId = :routeId', {routeId})
        .innerJoin('routePhoto.user', 'user')
        .innerJoin('routePhoto.route', 'route')
        .getMany();
    }

    public async getAllUsersPhotos(userId: number): Promise<IRoutePhoto[]> {
        return await getRepository(RoutePhoto)
        .createQueryBuilder('routePhoto')
        .select(['routePhoto', 'user', 'route.id'])
        .where('routePhoto.userId = :userId', {userId})
        .innerJoin('routePhoto.user', 'user')
        .innerJoin('routePhoto.route', 'route')
        .getMany();
    }

    public async add(id: number, userId: number, body: any): Promise<IRoutePhoto>  {
        const route = await getRepository(Route).findOne(id);
        const user = await getRepository(User).findOne(userId);
        const routePhotoRepository = await getRepository(RoutePhoto);

        const routephoto = new RoutePhoto();
        routephoto.user = user;
        routephoto.route = route;

        routephoto.caption = body.caption;
        routephoto.photo = body.photo;
        routephoto.coordinates = body.coordinates;
        routephoto.title = body.title;

        return await routePhotoRepository.save(routephoto);
    }

    public async delete(id: number, photoId: number): Promise<IRoutePhoto>  {
        const routePhotoRepository = await getRepository(RoutePhoto);

        const routePhoto = await routePhotoRepository.findOne(photoId);
        if (!routePhoto) {
            throw boom.notFound('Route photo Not Found');
        }

        return await routePhotoRepository.remove(routePhoto).then((result) => {
            return result;
        }).catch((err) => {
                throw boom.notFound('Could not delete route photo.', err);
        });
    }

    public async update(id: number, photoId: number, body: any): Promise<IRoutePhoto>  {
        const routePhotoRepository = await getRepository(RoutePhoto);

        const routePhoto = await routePhotoRepository.findOne(photoId);

        if (!routePhoto) {
            throw boom.notFound('router Photo Not Found');
        }

        routePhoto.caption = body.caption;

        return await routePhotoRepository.save(routePhoto);
    }
}
