import { IUser } from '../../models/User';
import { IRoutePhoto } from '../../models/RoutePhoto';

export interface IUserService {
    findOne(id: number): Promise<IUser>;
    findAll(): Promise<IUser[]>;
    getUserByUsernameAndPassword(username: string, password: string): Promise<IUser>;
    add(body: any): Promise<IUser>;
    delete(id: number): Promise<IUser>;
    update(id: number, body: any): Promise<IUser>;
}
