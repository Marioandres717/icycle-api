import { IRoutePhoto } from '../../models/RoutePhoto';

export interface IRoutePhotoService {
    findOne(id: number, commentId: number): Promise<IRoutePhoto>;
    findAll(id: number): Promise<IRoutePhoto[]>;
    add(id: number, userId: number, body: any): Promise<IRoutePhoto>;
    delete(id: number, commentId: number): Promise<IRoutePhoto>;
    update(id: number, commentId: number, body: any): Promise<IRoutePhoto>;
    getAllUsersPhotos(userId: number): Promise<IRoutePhoto[]>;
}
