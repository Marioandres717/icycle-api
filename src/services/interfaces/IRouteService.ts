import { IRoute } from '../../models/Route';

export interface IRouteService {
    findOne(id: number): Promise<IRoute>;
    findAll(): Promise<IRoute[]>;
    findUserRoutes(userId: number): Promise<IRoute[]>;
    findUserSavedRoutes(userId: number): Promise<IRoute[]>;
    saveRoute(id: number, userId: number): Promise<any>;
    add(body: any): Promise<IRoute>;
    delete(id: number): Promise<IRoute>;
    update(id: number, body: any): Promise<IRoute>;
    upVoteRoute(id: number, userId: number): Promise<IRoute>;
    downVoteRoute(id: number, userId: number): Promise<IRoute>;
    hasVoted(id: number, userId: number): Promise<any>;
    hasSaved(id: number, userId: number): Promise<any>;
}
