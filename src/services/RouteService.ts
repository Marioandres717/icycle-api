import { getRepository } from 'typeorm';
import * as boom from 'boom';
import { IRouteService } from '../services/interfaces/IRouteService';
import { IRoute, Route } from '../models/Route';
import { User } from '../models/User';
export class RouteService implements IRouteService {

    public async findOne(id: number): Promise<IRoute>  {
        const route = await getRepository(Route)
                        .createQueryBuilder('route')
                        .where('route.id = :id', {id})
                        .leftJoinAndSelect('route.user', 'user')
                        .getOne();
        if (!route) {
            throw boom.notFound('Route Not Found');
        }

        return route;
    }

    public async findAll(): Promise<IRoute[]>  {
        return await getRepository(Route)
        .createQueryBuilder('route')
        .where('route.private = false')
        .leftJoinAndSelect('route.user', 'user')
        .getMany();
    }

    // Find the routes a user has saved
    public async findUserSavedRoutes(userId: number): Promise<IRoute[]> {
        const userRepository = await getRepository(User);

        const user = await userRepository.createQueryBuilder('user')
        .where('user.id = :userId', {userId})
        .leftJoinAndSelect('user.savedRoutes', 'savedRoutes')
        .leftJoinAndSelect('savedRoutes.user', 'routeUser')
        .getOne();

        console.log(user);

        const savedRoutes = user.savedRoutes;

        return savedRoutes;
    }

    public async findUserRoutes(userId: number): Promise<IRoute[]> {
        return await getRepository(Route)
        .createQueryBuilder('route')
        .where('route.userId = :userId', {userId})
        .leftJoinAndSelect('route.user', 'user')
        .getMany();
    }

    public async saveRoute(id: number, userId: number): Promise<any> {
        const userRepository = await getRepository(User);
        const routeRepository = await getRepository(Route);

        const route = await routeRepository.findOne(id);

        const userWithMinifiedVotes = await userRepository.createQueryBuilder('user')
        .where('user.id = :userId', {userId})
        .leftJoinAndSelect('user.savedRoutes', 'savedRoutes', 'savedRoutes.id = :id', {id})
        .getOne();

        console.log(userWithMinifiedVotes);

        const user = await userRepository.createQueryBuilder('user')
        .where('user.id = :userId', {userId})
        .leftJoinAndSelect('user.savedRoutes', 'savedRoutes')
        .getOne();

        if (userWithMinifiedVotes.savedRoutes.length === 0) { // User does not have the route saved, so save it
            user.savedRoutes = user.savedRoutes.concat([route]);

            await userRepository.save(user);

            return true;
        } else { // User has the route saved, so remove it
            const filteredSavedRoutes = user.savedRoutes.filter((value, index, arr) => {
                return value.id !== route.id;
            });

            user.savedRoutes = filteredSavedRoutes;

            await userRepository.save(user);

            return true;
        }
    }

    public async add(body: any): Promise<IRoute>  {
        const routeRepository = await getRepository(Route);

        const user = await getRepository(User).findOne(body.userId);
        if (!user) {
            throw boom.notFound('User Not Found');
        }

        const route = new Route();
        route.title = body.title;
        route.routePins = body.routePins;
        route.pointPins = body.pointPins;
        route.difficulty = body.difficulty;
        route.private = body.private;
        route.note = body.note;
        route.user = user;
        route.distance = body.distance;

        const createdRoute = await routeRepository.save(route);
        return createdRoute;
    }

    public async delete(id: number): Promise<IRoute>  {
        const routeRepository = await getRepository(Route);

        const route = await routeRepository.findOne(id);
        if (!Route) {
            throw boom.notFound('Route Not Found');
        }

        return await routeRepository.remove(route).then((result) => {
            return result;
        }).catch((err) => {
                throw boom.notFound('Could not delete Route.', err);
        });
    }

    public async update(id: number, body: any): Promise<IRoute>  {
        const routeRepository = await getRepository(Route);

        const route = await routeRepository.findOne(id);

        if (!route) {
            throw boom.notFound('Route Not Found');
        }

        route.title = body.title;
        route.routePins = body.routePins;
        route.difficulty = body.difficulty;
        route.private = body.private;
        route.note = body.note;
        route.distance = body.distance;

        return await routeRepository.save(route);
    }

    public async upVoteRoute(id: number, userId: number): Promise<any> {
        const routeRepository = await getRepository(Route);
        const userRepository = await getRepository(User);

        const route = await routeRepository.findOne(id);

        const userWithMinifiedVotes = await userRepository.createQueryBuilder('user')
        .where('user.id = :userId', {userId})
        .leftJoinAndSelect('user.upVotedRoutes', 'upVotedRoutes', 'upVotedRoutes.id = :id', {id})
        .leftJoinAndSelect('user.downVotedRoutes', 'downVotedRoutes', 'downVotedRoutes.id = :id', {id})
        .getOne();

        const user = await userRepository.createQueryBuilder('user')
        .where('user.id = :userId', {userId})
        .leftJoinAndSelect('user.upVotedRoutes', 'upVotedRoutes')
        .leftJoinAndSelect('user.downVotedRoutes', 'downVotedRoutes')
        .getOne();

        console.log(userWithMinifiedVotes);

        if (userWithMinifiedVotes.upVotedRoutes.length === 1 && userWithMinifiedVotes.downVotedRoutes.length === 0) { // They have already upVoted, so remove the vote.
            console.log(1);
            const filteredUpVotes = user.upVotedRoutes.filter((value, index, arr) => {
                return value.id !== route.id;
            });

            user.upVotedRoutes = filteredUpVotes;
            route.upVotes--;
            await userRepository.save(user);
            await routeRepository.save(route);

            return true;
        } else if (userWithMinifiedVotes.downVotedRoutes.length === 1 && userWithMinifiedVotes.upVotedRoutes.length === 0) { // They have downVoted, so remove the downvote, and add an upvote
            console.log(2);
            const filteredDownVotes = user.downVotedRoutes.filter((value, index, arr) => {
                return value.id !== route.id;
            });
            user.downVotedRoutes = filteredDownVotes; // Remove DownVote
            user.upVotedRoutes = user.upVotedRoutes.concat([route]); // Add UpVote
            route.downVotes--;
            route.upVotes++;

            await userRepository.save(user);
            await routeRepository.save(route);

            return true;
        } else if (userWithMinifiedVotes.upVotedRoutes.length === 0 && userWithMinifiedVotes.downVotedRoutes.length === 0) { // They have not voted
            console.log(3);
            user.upVotedRoutes = user.upVotedRoutes.concat([route]);
            await userRepository.save(user);

            route.upVotes++;
            await routeRepository.save(route);
            return true;
        }
    }

    public async downVoteRoute(id: number, userId: number): Promise<any> {
        const routeRepository = await getRepository(Route);
        const userRepository = await getRepository(User);

        const route = await routeRepository.findOne(id);

        const userWithMinifiedVotes = await userRepository.createQueryBuilder('user')
        .where('user.id = :userId', {userId})
        .leftJoinAndSelect('user.upVotedRoutes', 'upVotedRoutes', 'upVotedRoutes.id = :id', {id})
        .leftJoinAndSelect('user.downVotedRoutes', 'downVotedRoutes', 'downVotedRoutes.id = :id', {id})
        .getOne();

        const user = await userRepository.createQueryBuilder('user')
        .where('user.id = :userId', {userId})
        .leftJoinAndSelect('user.upVotedRoutes', 'upVotedRoutes')
        .leftJoinAndSelect('user.downVotedRoutes', 'downVotedRoutes')
        .getOne();

        console.log(userWithMinifiedVotes);

        if (userWithMinifiedVotes.downVotedRoutes.length === 1 && userWithMinifiedVotes.upVotedRoutes.length === 0) { // They have already downVoted, so remove the vote.
            console.log(1);
            const filteredDownVotes = user.downVotedRoutes.filter((value, index, arr) => {
                return value.id !== route.id;
            });

            user.downVotedRoutes = filteredDownVotes;
            route.downVotes--;
            await userRepository.save(user);
            await routeRepository.save(route);

            return true;
        } else if (userWithMinifiedVotes.upVotedRoutes.length === 1 && userWithMinifiedVotes.downVotedRoutes.length === 0) { // They have upVoted, so remove the upVote, and add an downVote
            console.log(2);
            const filteredUpVotes = user.upVotedRoutes.filter((value, index, arr) => {
                return value.id !== route.id;
            });
            user.upVotedRoutes = filteredUpVotes; // Remove DownVote
            user.downVotedRoutes = user.downVotedRoutes.concat([route]); // Add UpVote
            route.upVotes--;
            route.downVotes++;

            await userRepository.save(user);
            await routeRepository.save(route);

            return true;
        } else if (userWithMinifiedVotes.downVotedRoutes.length === 0 && userWithMinifiedVotes.upVotedRoutes.length === 0) { // They have not voted
            console.log(3);
            user.downVotedRoutes = user.downVotedRoutes.concat([route]);
            await userRepository.save(user);

            route.downVotes++;
            await routeRepository.save(route);
            return true;
        }
    }

    public async hasVoted(id: number, userId: number): Promise<any> {
        const routeRepository = await getRepository(Route);
        const userRepository = await getRepository(User);

        const user = await userRepository.createQueryBuilder('user')
        .where('user.id = :userId', {userId})
        .leftJoinAndSelect('user.upVotedRoutes', 'upVotedRoutes', 'upVotedRoutes.id = :id', {id})
        .leftJoinAndSelect('user.downVotedRoutes', 'downVotedRoutes', 'downVotedRoutes.id = :id', {id})
        .getOne();

        if (user.upVotedRoutes.length === 1) {
            return({voted: 'up'});
        } else if (user.downVotedRoutes.length === 1 ) {
            return({voted: 'down'});
        } else {
            return({voted: 'none'});
        }
    }

    public async hasSaved(id: number, userId: number): Promise<any> {
        const routeRepository = await getRepository(Route);
        const userRepository = await getRepository(User);

        const user = await userRepository.createQueryBuilder('user')
        .where('user.id = :userId', {userId})
        .leftJoinAndSelect('user.savedRoutes', 'savedRoutes', 'savedRoutes.id = :id', {id})
        .getOne();

        if (user.savedRoutes.length === 1) {
            return({saved: true});
        } else {
            return({voted: false});
        }
    }
}
