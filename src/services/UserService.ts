import { IUserService } from './interfaces/IUserService';
import { IUser, User} from '../models/User';
import { getRepository } from 'typeorm';
import * as boom from 'boom';
import { IRoutePhoto, RoutePhoto } from '../models/RoutePhoto';

export class UserService implements IUserService {

    // Find a single user
    public async findOne(id: number): Promise<IUser>  {
        const user = await getRepository(User).findOne(id);
        if (!user) {
            throw boom.notFound('User Not Found');
        }
        return user;
    }

    // Find the user by username and password
    public async getUserByUsernameAndPassword(username: string, password: string): Promise<IUser> {
        const user = await getRepository(User)
                    .createQueryBuilder('user')
                    .where('user.username = :username', {username})
                    .andWhere('user.password = :password', {password})
                    .getOne();
        if (!user) {
            throw boom.notFound('User Not Found');
        }

        return user;
    }

    // Find all users
    public async findAll(): Promise<IUser[]>  {
        return await getRepository(User)
        .createQueryBuilder('user')
        .getMany();
    }

    // Create a user
    public async add(body: any): Promise<IUser>  {
        const userRepository = await getRepository(User);
        const user = new User();
        user.username = body.username;
        user.password = body.password;

        const createdUser = await userRepository.save(user);
        return createdUser;
    }

    // Delete a user
    public async delete(id: number): Promise<IUser>  {
        const userRepository = await getRepository(User);
        const user = await userRepository.findOne(id);

        if (!user) {
            throw boom.notFound('User Not Found');
        }

        return await userRepository.remove(user).then((result) => {
            return result;
        }).catch((err) => {
                throw boom.notFound('Could not delete user.', err);
        });
    }

    // Update a user
    public async update(id: number, body: IUser): Promise<IUser>  {
        const userRepository = await getRepository(User);

        const user = await userRepository.findOne(id);

        if (!user) {
            throw boom.notFound('User Not Found');
        }

        if (body.bikeBrand && body.bikeBrand !== user.bikeBrand) {
            user.bikeBrand = body.bikeBrand;
        }

        if (body.bikeSerialNumber && body.bikeSerialNumber !== user.bikeSerialNumber) {
            user.bikeSerialNumber = body.bikeSerialNumber;
        }

        if (body.bikeNotes && body.bikeNotes !== user.bikeNotes) {
            user.bikeNotes = body.bikeNotes;
        }

        if (body.bikePhoto && body.bikePhoto !== user.bikePhoto) {
            user.bikePhoto = body.bikePhoto;
        }

        return await userRepository.save(user);
    }

    // Find all the photos a user has uploaded to any route
    public async findUserPhotos(id: number): Promise<IRoutePhoto[]> {
        const routePhotoRepository = await getRepository(RoutePhoto);
        const user = await getRepository(User).findOne(id);

        const photos = await  routePhotoRepository.createQueryBuilder('routePhoto')
            .leftJoinAndSelect('routePhoto.user', 'user')
            .where('routePhoto.user = :id', {user})
            .getMany();

        return photos;
    }
}
