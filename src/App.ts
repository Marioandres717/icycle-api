import * as express from 'express';
import * as bodyParser from 'body-parser';
import * as logger from 'morgan';
import * as boom from 'boom';
import { UserController } from './routes/UserController';
import { UserService } from './services/UserService';
import { RouteController } from './routes/RouteController';
import { RouteService } from './services/RouteService';
import { RoutePhotoController } from './routes/RoutePhotoController';
import { RoutePhotoService } from './services/RoutePhotoService';

class App {
  public express: express.Application;

  constructor() {
    this.express = express();
    this.express.disable('x-powered-by');
    this.express.enable('trust proxy');

    // enable cors
    // tslint:disable-next-line
    this.express.use(
      (
        req: express.Request,
        res: express.Response,
        next: express.NextFunction,
      ) => {
        res.header('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE');
        res.header('Access-Control-Allow-Origin', '*');
        res.header(
          'Access-Control-Allow-Headers',
          'Origin, X-Requested-With, Content-Type, Accept, Authorization, Cache-Control, Pragma, Expires',
        );
        next();
      },
    );

    this.middleware();
    this.routes();
    this.errorHandler();
  }

  private middleware(): void {
    this.express.use(logger('dev'));
    this.express.use(bodyParser.json({limit: '2mb'}));
    this.express.use(bodyParser.urlencoded({ extended: false }));

    this.express.use((req: express.Request, res: express.Response, next: express.NextFunction) => {
      const then = Date.now();
      res.on('finish', () => {
        const log = {
          duration: Date.now() - then,
          method: req.method,
          params: req.params,
          path: req.route ? req.route.path : null,
          statusCode: res.statusCode,
          statusMessage: res.statusMessage,
          timestamp: then,
          type: 'request_log',
          url: req.url,
        };
        console.log(JSON.stringify(log));
      });
      next();
    });
  }

  private routes(): void {
    const router = express.Router();
    UserController.create(router, new UserService());
    RouteController.create(router, new RouteService());
    RoutePhotoController.create(router, new RoutePhotoService());
    this.express.use(router);
  }

  private errorHandler(): void {
    // tslint:disable-next-line
    this.express.use((error: any, req: express.Request, res: express.Response, next: express.NextFunction) => {

      // Is not a boom formatted error.
      if (!error.output) {
        boom.boomify(error, {
          message: error.message,
          statusCode: error.status,
        });
      }

      console.error(error);
      res.status(error.output.statusCode).send(error.output.payload);
    });
  }
}

export default new App().express;
